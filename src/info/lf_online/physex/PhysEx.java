package info.lf_online.physex;

import info.lf_online.physex.exercises.ConversionExercise;
import info.lf_online.physex.exercises.EquationExercise;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;

/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 10.05.2015 at 01:30:25.
 * All rights reserved (C) 2014.
 *
 * Class-Info: Main- Initiates / Loads whole Program
 *
 * @author See
 */
public class PhysEx extends JFrame
{
	private static final long serialVersionUID = 7616569056680508483L;
	private static PhysEx instance;
	
	public PhysEx()
	{
		super("PhysEx - Physics Exercises > Menu");
		
		setIconImage(loadImage("progico.png"));
		setNimbusLookAndFeel();
		
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setResizable(false);
		
		add(Box.createVerticalStrut(20));
		JLabel logo = new JLabel(new ImageIcon(loadImage("progico.png").getScaledInstance(200, 200, 0)), SwingConstants.CENTER);
		logo.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(logo);
		
		JLabel credits = new JLabel("Coded by Laurin F�ller.", SwingConstants.CENTER);
		credits.setMaximumSize(new Dimension(400, 30));
		credits.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(credits);
		
		add(Box.createVerticalStrut(20));
		
		JButton equation = new JButton("Gleichungsumstellungsaufgaben");
		equation.setMaximumSize(new Dimension(400, 30));
		equation.setAlignmentX(Component.CENTER_ALIGNMENT);
		equation.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new EquationExercise();
				setVisible(false);
				dispose();
			}
		});
		
		add(equation);
		
		add(Box.createVerticalStrut(10));
		
		JButton convert = new JButton("Umwandlungsaufgaben");
		convert.setMaximumSize(new Dimension(400, 30));
		convert.setAlignmentX(Component.CENTER_ALIGNMENT);
		convert.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				new ConversionExercise();
				setVisible(false);
				dispose();
			}
		});
		
		add(convert);
		
		add(Box.createVerticalStrut(10));
		
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setSize(640, 480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static BufferedImage loadImage(String imagePath)
	{
		URL url = PhysEx.class.getResource("/res/"+imagePath);
		BufferedImage image = null;
		try
		{
			image = ImageIO.read(url);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return image;
	}
	
	public static BufferedReader loadTxtStream(String txtPath)
	{
        InputStream stream = PhysEx.class.getResourceAsStream("/res/"+txtPath);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        
        try {
        	reader = new BufferedReader(new InputStreamReader(stream));
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        return reader;
	}
	
	void setNimbusLookAndFeel()
	{
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		    // If Nimbus is not available, fall back to cross-platform
		    try {
		        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		    } catch (Exception ex) {
		        // not worth my time
		    }
		}
	}
	
	public static void main(String[] args)
	{
		instance = new PhysEx();
	}
	
	public static PhysEx getInstance()
	{
		return instance;
	}
}
