package info.lf_online.physex.exercises;


/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 13.05.2015 at 00:11:11.
 * All rights reserved (C) 2014.
 *
 * Class-Info: JFrame showing exercises refering to equations
 *
 * @author See
 */
public class EquationExercise extends ExerciseFrame
{
	private static final long serialVersionUID = -1290218913443699892L;

	public EquationExercise()
	{
		super(EquationExercise.class, "equations.txt");
	}
}
