package info.lf_online.physex.exercises;

import javax.measure.quantity.Area;
import javax.measure.quantity.Length;
import javax.measure.quantity.Pressure;
import javax.measure.quantity.Volume;
import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.Unit;

/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 13.05.2015 at 20:49:42.
 * All rights reserved (C) 2014.
 *
 * Class-Info: handles conversion
 *
 * @author See
 */
public class Measurement
{
	private static final Unit<Length> M = SI.METER;
	private static final Unit<Length> KM = SI.KILOMETER;
	private static final Unit<Length> DM = SI.DECI(M);
	private static final Unit<Length> CM = SI.CENTIMETER;
	private static final Unit<Length> MM = SI.MILLIMETER;
	
	private static final Unit<Volume> CUBIC_M = SI.CUBIC_METRE;
	private static final Unit<Volume> CUBIC_KM = SI.KILO(CUBIC_M);
	private static final Unit<Volume> CUBIC_DM = SI.DECI(CUBIC_M);
	private static final Unit<Volume> CUBIC_CM = SI.CENTI(CUBIC_M);
	private static final Unit<Volume> CUBIC_MM = SI.MILLI(CUBIC_M);
	
	private static final Unit<Area> SQUARE_M = SI.SQUARE_METRE;
	private static final Unit<Area> SQUARE_KM = SI.KILO(SQUARE_M);
	private static final Unit<Area> SQUARE_DM = SI.DECI(SQUARE_M);
	private static final Unit<Area> SQUARE_CM = SI.CENTI(SQUARE_M);
	private static final Unit<Area> SQUARE_MM = SI.MILLI(SQUARE_M);
	
	private static final Unit<Pressure> PA = SI.PASCAL;
	private static final Unit<Pressure> MPA = SI.MEGA(PA);
	private static final Unit<Pressure> HPA = SI.HECTO(PA);
	private static final Unit<Pressure> BAR = NonSI.BAR;
	
	public enum MeasureLength
	{
		KM(Measurement.KM),
		M(Measurement.M),
		DM(Measurement.DM),
		CM(Measurement.CM),
		MM(Measurement.MM);
		
		Unit<Length> unit;
		
		private MeasureLength(Unit<Length> unit)
		{
			this.unit = unit;
		}
		
		public Unit<Length> getUnit()
		{
			return unit;
		}
		
		public String getName()
		{
			return toString().toLowerCase();
		}
	}
	
	public enum MeasureVolume
	{
		CUBIC_M(Measurement.CUBIC_KM, "m�"),
		CUBIC_KM(Measurement.CUBIC_M, "km�"),
		CUBIC_DM(Measurement.CUBIC_DM, "dm�"),
		CUBIC_CM(Measurement.CUBIC_CM, "cm�"),
		CUBIC_MM(Measurement.CUBIC_MM, "mm�");
		
		Unit<Volume> unit;
		String name;
		
		private MeasureVolume(Unit<Volume> unit, String name)
		{
			this.unit = unit;
			this.name = name;
		}
		
		public Unit<Volume> getUnit()
		{
			return unit;
		}
		
		public String getName()
		{
			return name;
		}
	}
	
	public enum MeasureArea
	{
		SQUARE_KM(Measurement.SQUARE_KM, "km3"),
		SQUARE_M(Measurement.SQUARE_M, "m�"),
		SQUARE_DM(Measurement.SQUARE_DM, "dm�"),
		SQUARE_CM(Measurement.SQUARE_CM, "cm�"),
		SQUARE_MM(Measurement.SQUARE_MM, "mm�");
		
		Unit<Area> unit;
		String name;
		
		private MeasureArea(Unit<Area> unit, String name)
		{
			this.unit = unit;
			this.name = name;
		}
		
		public Unit<Area> getUnit()
		{
			return unit;
		}
		
		public String getName()
		{
			return name;
		}
	}
	
	public enum MeasurePressure
	{
		N_PER_CUBIC_CM(Measurement.PA, "N/cm�"),
		PA(Measurement.PA, "Pa"),
		MPA(Measurement.MPA, "MPa"),
		HPA(Measurement.HPA, "HPa"),
		BAR(Measurement.BAR, "Bar");
		
		Unit<Pressure> unit;
		String name;
		
		private MeasurePressure(Unit<Pressure> unit, String name)
		{
			this.unit = unit;
			this.name = name;
		}
		
		public Unit<Pressure> getUnit()
		{
			return unit;
		}
		
		public String getName()
		{
			return name;
		}
	}
	
}
