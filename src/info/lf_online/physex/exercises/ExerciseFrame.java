package info.lf_online.physex.exercises;

import info.lf_online.physex.PhysEx;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 13.05.2015 at 12:56:42.
 * All rights reserved (C) 2014.
 *
 * Class-Info:
 *
 * @author See
 */
public class ExerciseFrame extends JFrame
{
	private static final long serialVersionUID = -2327285861565623615L;
	
	private Exercise currExercise;
	private List<Exercise> exercises;
	private int questionCount = 0;
	
	private JLabel exercise;
	private JPanel buttonField;
	private JTextField answerField;
	private JButton enterButton;
	private JButton backButton;
	
	public ExerciseFrame(Class<?> classObject, List<Exercise> exercises)
	{
		super("PhysEx - Physics Exercises > "+classObject.getSimpleName());
		loadFrame(null, exercises, false);
	}
	
	public ExerciseFrame(Class<?> classObject, String exerciseTxtPath)
	{
		super("PhysEx - Physics Exercises > "+classObject.getSimpleName());
		loadFrame(exerciseTxtPath, null, true);
	}
	
	void loadFrame(String exerciseTxtPath, List<Exercise> exercises, boolean exerciseTxtPathEnabled) // if exerciseTxtPath - exercises from file - else from List
	{
		setIconImage(PhysEx.loadImage("progico.png"));
		
		if(exerciseTxtPathEnabled)
		{
			this.exercises = loadExercises(exerciseTxtPath);
		} else { this.exercises = exercises; }
		
		exercise = new JLabel();
		answerField = new JTextField();
		enterButton = new JButton("Eingabe");
		backButton = new JButton("Zur�ck");
		buttonField = new JPanel();
		
		buttonField.setLayout(new BoxLayout(buttonField, BoxLayout.X_AXIS));
		
		add(Box.createVerticalStrut(120));
		JLabel logo = new JLabel(new ImageIcon(PhysEx.loadImage("progico.png").getScaledInstance(100, 100, 0)), SwingConstants.CENTER);
		logo.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(logo);
		add(Box.createVerticalStrut(20));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setResizable(false);
		
		exercise.setMaximumSize(new Dimension(400, 30));
		exercise.setAlignmentX(Component.CENTER_ALIGNMENT);
		exercise.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
		exercise.setBounds(new Rectangle(400, 30));
		exercise.setLocation( (int) exercise.getLocation().getX(), (int) exercise.getLocation().getY()-20);
		
		add(exercise);
		
		answerField.setMaximumSize(new Dimension(400, 30));
		answerField.setAlignmentX(Component.CENTER_ALIGNMENT);
		answerField.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				showDialog(currExercise.checkAnswer(answerField.getText()));
			}
		});
		
		buttonField.add(answerField);
		buttonField.add(enterButton);
		enterButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				showDialog(currExercise.checkAnswer(answerField.getText()));
			}
		});
		
		add(Box.createVerticalStrut(10));
		add(buttonField);
		
		backButton.setMaximumSize(new Dimension(480, 30));
		backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		backButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				setVisible(false);
				dispose();
				new PhysEx();
			}
		});
		
		add(backButton);
		
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setSize(640, 480);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
		
		nextExercise();
	}
	
	private void showDialog(boolean b)
	{
		if(b) { JOptionPane.showMessageDialog(null, "Aufgabe Richtig!"); nextExercise(); } 
		else { JOptionPane.showMessageDialog(null, "Aufgabe Falsch. Richtiges Ergebnis w�re: "+currExercise.getTrueAnswers()[0]); }
	}
	
	public void nextExercise()
	{	
		questionCount++;
		if(!exercises.isEmpty())
		{	
			currExercise = exercises.get(new Random().nextInt(exercises.size()));
			
			String exerciseText = "<html>";
			for(String part : getParts("Aufgabe "+questionCount+": "+currExercise.getText()+": "+currExercise.getExercise(), 100))
			{
				exerciseText = exerciseText+"<br>"+part;
			}
			exerciseText = exerciseText+"<html>";
			exercise.setText(exerciseText);
			answerField.setText(null);
			
			exercises.remove(currExercise);
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Alle Aufgaben erledigt.");
			setVisible(false);
			dispose();
			new PhysEx();
		}
	}
	
	public List<Exercise> loadExercises(String txtPath)
	{
		BufferedReader exRead = PhysEx.loadTxtStream(txtPath);
		List<Exercise> toReturn = new ArrayList<Exercise>();
		
		try 
		{
			String nextLine = exRead.readLine();
			
			while(nextLine != null)
			{
				toReturn.add(Exercise.parseExercise(nextLine));
				nextLine = exRead.readLine();
			}
		} catch (IOException e) { e.printStackTrace();}
		
		return toReturn;
	}
	
    private static List<String> getParts(String string, int partitionSize) 
    {
        List<String> parts = new ArrayList<String>();
        int len = string.length();
        for (int i=0; i<len; i+=partitionSize)
        {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }
        return parts;
    }
	
	public Exercise getCurrentExercise()
	{
		return currExercise;
	}
	
	public List<Exercise> getExercises()
	{
		return exercises;
	}
	
	public void setExercises(List<Exercise> toSet)
	{
		exercises = toSet;
	}
}
