package info.lf_online.physex.exercises;

import java.util.Arrays;

/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 13.05.2015 at 00:44:23.
 * All rights reserved (C) 2014.
 *
 * Class-Info: 
 *
 * @author See
 */
public class Exercise
{
	private String text, exercise;
	private String[] trueAnswers;
	
	public Exercise(String text, String exercise, String ... trueAnswers)
	{
		this.text = text;
		this.exercise = exercise;
		this.trueAnswers = trueAnswers;
	}
	
	public boolean checkAnswer(String answer)
	{
		return Arrays.asList(trueAnswers).contains(answer);
	}
	
	public String getText()
	{
		return text;
	}
	
	public String getExercise()
	{
		return exercise;
	}
	
	public String[] getTrueAnswers()
	{
		return trueAnswers;
	}
	
	@Override
	public String toString()
	{
		return text+"#"+exercise+"#"+Arrays.asList(trueAnswers).toString();
	}
	
	public static Exercise parseExercise(String str)
	{
		String[] partedExercise = str.split("#");
		
		return new Exercise(partedExercise[0], partedExercise[1], partedExercise[2].split(";"));
	}
}