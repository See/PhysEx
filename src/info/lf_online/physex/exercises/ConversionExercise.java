package info.lf_online.physex.exercises;

import info.lf_online.physex.exercises.Measurement.MeasureArea;
import info.lf_online.physex.exercises.Measurement.MeasureLength;
import info.lf_online.physex.exercises.Measurement.MeasurePressure;
import info.lf_online.physex.exercises.Measurement.MeasureVolume;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * PhysEx - Physics Exercises - Simple Physics-Training-Program.
 * Coded by Laurin "See" F�ller 2015.
 * Classcreation on the 13.05.2015 at 00:08:26.
 * All rights reserved (C) 2014.
 *
 * Class-Info:
 *
 * @author See
 */
public class ConversionExercise extends ExerciseFrame
{
	public ConversionExercise()
	{
		super(ConversionExercise.class, loadExercises());
	}

	private static List<Exercise> loadExercises()
	{
		List<Exercise> toReturn = new ArrayList<Exercise>();
		
		for(int i=0; i < 15; i++)
		{
			toReturn.add(getRandomExercise());
		}
		
		return toReturn;
	}
	
	private static Exercise getRandomExercise()
	{
		int rand = new Random().nextInt(4)+1;
		
		switch(rand)
		{
		case 1: return generateAreaExercise();
		case 2: return generatePressureExercise();
		case 3: return generateVolumeExercise();
		case 4: return generateLengthExercise();
		}
		
		return null;
	}
	
	private static Exercise generateAreaExercise()
	{
		Random genRandom = new Random();
		
		MeasureArea u1 = MeasureArea.values()[genRandom.nextInt(MeasureArea.values().length)];
		MeasureArea u2 = null;
		
		int numberToConvert = genRandom.nextInt(50)+1;
		
		while(u2 == null && u2 != u1)
		{
			u2 = MeasureArea.values()[genRandom.nextInt(MeasureArea.values().length)];	
		}
		
		return new Exercise("Rechne um", numberToConvert+u1.getName()+" in "+u2.getName(), new String(u1.getUnit().getConverterTo(u2.getUnit()).convert(numberToConvert)+"").replace(".0", ""));
	}
	
	private static Exercise generatePressureExercise()
	{
		Random genRandom = new Random();
		
		MeasurePressure u1 = MeasurePressure.values()[genRandom.nextInt(MeasurePressure.values().length)];
		MeasurePressure u2 = null;
		
		int numberToConvert = genRandom.nextInt(50)+1;
		
		while(u2 == null && u2 != u1)
		{
			u2 = MeasurePressure.values()[genRandom.nextInt(MeasurePressure.values().length)];	
		}
		
		return new Exercise("Rechne um", numberToConvert+u1.getName()+" in "+u2.getName(), new String(u1.getUnit().getConverterTo(u2.getUnit()).convert(numberToConvert)+"").replace(".0", ""));
	}
	
	private static Exercise generateVolumeExercise()
	{
		Random genRandom = new Random();
		
		MeasureVolume u1 = MeasureVolume.values()[genRandom.nextInt(MeasureVolume.values().length)];
		MeasureVolume u2 = null;
		
		int numberToConvert = genRandom.nextInt(50)+1;
		
		while(u2 == null && u2 != u1)
		{
			u2 = MeasureVolume.values()[genRandom.nextInt(MeasureVolume.values().length)];	
		}
		
		return new Exercise("Rechne um", numberToConvert+u1.getName()+" in "+u2.getName(), new String(u1.getUnit().getConverterTo(u2.getUnit()).convert(numberToConvert)+"").replace(".0", ""));
	}
	
	private static Exercise generateLengthExercise()
	{
		Random genRandom = new Random();
		
		MeasureLength u1 = MeasureLength.values()[genRandom.nextInt(MeasureLength.values().length)];
		MeasureLength u2 = null;
		
		int numberToConvert = genRandom.nextInt(50)+1;
		
		while(u2 == null && u2 != u1)
		{
			u2 = MeasureLength.values()[genRandom.nextInt(MeasureLength.values().length)];	
		}
		
		return new Exercise("Rechne um", numberToConvert+u1.getName()+" in "+u2.getName(), new String(u1.getUnit().getConverterTo(u2.getUnit()).convert(numberToConvert)+"").replace(".0", ""));
	}
	
	private static final long serialVersionUID = -4167057151130975853L;
}
